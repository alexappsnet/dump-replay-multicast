#!/usr/bin/env python3

from datetime import datetime
import socket
import time

BIND_NIC = '10.0.0.101'
MCAST_GRP = '224.123.45.1'
MCAST_PORT = 5007

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_IF, socket.inet_aton(BIND_NIC))
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)

while True:
    data = str(datetime.now())
    sock.sendto(data.encode(), (MCAST_GRP, MCAST_PORT))
    time.sleep(0.1)
