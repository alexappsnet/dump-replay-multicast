#!/usr/bin/env python3

import socket
import struct

BIND_NIC = '10.0.0.102'
MCAST_GRP = '224.123.45.1'
MCAST_PORT = 5007

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_IF, socket.inet_aton(BIND_NIC))
sock.bind(('', MCAST_PORT))
sock.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP, socket.inet_aton(MCAST_GRP) + socket.inet_aton(BIND_NIC))

while True:
    data = sock.recv(10240)
    print(data)
